# Task 2.1 - Broken Links Test

## Project Specific Info

This project tests that links in the footer of https://www.vegasslotsonline.com/ work as expected by getting the href attribute and asserts that each URL returns a 200 OK response code.

## Project Overview
The projects written follow a top down approach. The lowest level is the page object. In this class we specify page objects such as web elements and methods which are directly related such as click and send keys. Methods which are more generic or can be used at different instances such as WebDriverWait and WebDriver are found in the Utilities class. This acts as a "mini-framework" for the project itself. 

The page manager level purpose is to join methods found at project level to compile different actions to create scenarios which use multiple methods from the object level. This my not be very useful for such small projects, however, can be critical when having more than one web page per test for example when testing full user journeys. This also applies to the asset manager, which is considered to be on the level as the page manager. The assert manager package contain class/es which do different assertions used objects/methods from the object level or Utilities class in the case of this project. 

Finally, the highest level is the main package, which contains test classes. This is where everything comes together and data is fed to the underlying classes so a functionality can be tested. In this class the TestNG library has been used to structure the tests better by preparing the test prerequisites using the BeforeTest annotation and closing the web driver using the AfterTest annotation. TestNG was used since it can offer visual to the processes/tests running in the background whilst clearly indicating the failed test.  For both of these projects, test data has been provided by using the VM options and property file.

This approach was taken to make the project flexible and test data can easily changed/updated without the need of modifying hardcoded data.

## Getting Started

To make use of this project, a ``mvn clean install`` needs to be executed in the terminal to ensure that all dependencies
are downloaded after the project is clone for BB. For simplicity's sake, this project does not make use of frameworks or libraries which are not
available online.

## Running the tests

1. Clone the project and open it using an IDE.
2. Create a TestNG run/debug configuration. TestKind should be Class and the "BrokenLinksTest" class should be selected (IDE should autoselect this).
3. Include the browser and website URL vm options as shown below. 

``-Dbrowser=chrome
-DwebsiteUrl=http://derisredwebsite.com``

Kindly note that the supported **browser** vm option arguments are: chrome, firefox, ie, edge.

## How it works

The automated test is designed to assert that the links in the footer are not broken. Therefore, a webdriver instance is created 
and navigates to https://www.vegasslotsonline.com/ (or any URL inputted in the designated vm option argument). 

It scrolls down to the footer (closes the banner which pops up) and gets the HREF attribute of the list items found in the footer.
These HREFs are called using HttpURLConnection class and the response is parsed by the assert manager class. Depending on the response code
it is determined if the URL is broken or not, and thus the programme fails or passes each test accordingly.