package pagemanagers;


import assertmanagers.LandingPageAssert;
import pageobjects.LandingPage;


/** This class is responsible of
 *  joining all the actions specified
 *  at page object level to create an
 *  end-to-end journey which then results
 *  in a test case.
 */

public class LandingPageManager {

    public static LandingPage landingPage = new LandingPage();


    public static void browseFooterAndCloseBanner() {
        landingPage.scrollToFooter();
        landingPage.closeBanner();

    }



}
