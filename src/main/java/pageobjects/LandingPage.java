package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Driver;

import static utils.Utilities.getHrefAttributeContent;

/** This class is contains the elements
 *  and other page object found on a specific
 *  web page. Each class within this package
 *  represents a page.
 *
 *  This class also contains actions related
 *  to objects like click and sendKeys.
 */

public class LandingPage {

    private WebDriver driver = Driver.getWebDriver();

    private By bannerCloseTxt  = By.xpath("//*[@id=\"variation1_form\"]/div[2]/div/span");
    private By footerListItems = By.xpath("//*[@id=\"vegas-body\"]/footer/div[2]/div/div[3]/div[2]/ul");

    private By contactUsListItem        = By.xpath("//*[@id=\"vegas-body\"]/footer/div[2]/div/div[3]/div[2]/ul/li[1]/a");
    private By respGamblingListItem     = By.xpath("//*[@id=\"vegas-body\"]/footer/div[2]/div/div[3]/div[2]/ul/li[2]/a");
    private By fairnessTestingListItem  = By.xpath("//*[@id=\"vegas-body\"]/footer/div[2]/div/div[3]/div[2]/ul/li[3]/a");
    private By licenseBodiesListItem    = By.xpath("//*[@id=\"vegas-body\"]/footer/div[2]/div/div[3]/div[2]/ul/li[4]/a");
    private By sitemapListItem          = By.xpath("//*[@id=\"vegas-body\"]/footer/div[2]/div/div[3]/div[2]/ul/li[5]/a");
    private By privacyPolicyListItem    = By.xpath("//*[@id=\"vegas-body\"]/footer/div[2]/div/div[3]/div[2]/ul/li[6]/a");


    public void scrollToFooter(){
        WebElement footerList = driver.findElement(footerListItems);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", footerList);

    }

    public void closeBanner(){
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(bannerCloseTxt));

        driver.findElement(bannerCloseTxt).click();
    }

    public String getContactUsHref(){
       return getHrefAttributeContent(driver, contactUsListItem);
    }

    public String getResponsibleGamblingHref(){
        return getHrefAttributeContent(driver, respGamblingListItem);
    }

    public String getFairnessAndTestingHref(){
        return getHrefAttributeContent(driver, fairnessTestingListItem);
    }

    public String getLicensingBodiesAndRegulatorsHref(){
        return getHrefAttributeContent(driver, licenseBodiesListItem);
    }

    public String getSitemapHref(){
        return getHrefAttributeContent(driver, sitemapListItem);
    }

    public String getPrivacyPolicyHref(){
        return getHrefAttributeContent(driver, privacyPolicyListItem);
    }




}
