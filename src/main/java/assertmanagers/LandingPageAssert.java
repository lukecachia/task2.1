package assertmanagers;

import org.testng.Assert;
import pageobjects.LandingPage;
import utils.Utilities;

import java.io.IOException;

/** This class contains methods which assert
 *  required verifications and returns a result
 *  according to the expected result.
 */

public class LandingPageAssert {

    private static LandingPage landingPage = new LandingPage();

    public static void verifyContactUsLink() throws IOException {
        int response = Utilities.getResponseForUrl(landingPage.getContactUsHref());

        if(response != 200){
            Assert.fail("Contact us URL in footer might be broken! " + response + " response code received.");
        }
    }

    public static void verifyResponsibleGamblingLink() throws IOException {
        int response = Utilities.getResponseForUrl(landingPage.getResponsibleGamblingHref());

        if(response != 200){
            Assert.fail("Responsible Gambling URL in footer might be broken! " + response + " response code received.");
        }
    }

    public static void verifyFairnessAndTestingLink() throws IOException {
        int response = Utilities.getResponseForUrl(landingPage.getFairnessAndTestingHref());

        if(response != 200){
            Assert.fail("Fairness and Testing URL in footer might be broken! " + response + " response code received.");
        }
    }

    public static void verifyLicBodiesAndRegLink() throws IOException {
        int response = Utilities.getResponseForUrl(landingPage.getLicensingBodiesAndRegulatorsHref());

        if(response != 200){
            Assert.fail("Licensing Bodies and Regulators URL in footer might be broken! " + response + " response code received.");
        }
    }

    public static void verifySitemapLink() throws IOException {
        int response = Utilities.getResponseForUrl(landingPage.getSitemapHref());

        if(response != 200){
            Assert.fail("Sitemap URL in footer might be broken! " + response + " response code received.");
        }
    }

    public static void verifyPrivacyPolicyLink() throws IOException {
        int response = Utilities.getResponseForUrl(landingPage.getPrivacyPolicyHref());

        if(response != 200){
            Assert.fail("Privacy Policy URL in footer might be broken! " + response + " response code received.");
        }
    }


}
