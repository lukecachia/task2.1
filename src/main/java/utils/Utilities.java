package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * This class contains methods which are usually
 * placed in frameworks. Methods here do
 * functionalities which are used frequently
 * and are not directly related to a specific web page
 * this avoids code duplication and reduces bugs.
 */

public class Utilities {

    /**
     * Method which returns the content of the href
     * attribute of a particular element.
     *
     * @param driver current web driver.
     * @param element element containing the href attribute.
     * @return the URL found in the href attribute.
     */

    public static String getHrefAttributeContent(WebDriver driver, By element){
        return driver.findElement(element).getAttribute("href");
    }

    /**
     *
     * @param retrievedUrl string returned by the getHrefAttributeContent method.
     * @return returns a response code.
     * @throws IOException
     */

    public static Integer getResponseForUrl(String retrievedUrl) throws IOException {
        int response = 0;

        try {
            URL url = new URL(retrievedUrl);
            HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();

            httpCon.setRequestMethod("GET");
            httpCon.connect();

            response = httpCon.getResponseCode();


        } catch (NullPointerException e) {
            System.out.println("URL is null.");
        }

        return response;
    }
}
