package main;

import assertmanagers.LandingPageAssert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pagemanagers.LandingPageManager;
import utils.Driver;

import java.io.IOException;

@Test()
public class BrokenLinksTest {

    @BeforeTest
    public void initiateDriverAndSetupPage(){
        Driver.startWebDriver(System.getProperty("browser"));
        Driver.navigateTo(System.getProperty("websiteURL"));

        LandingPageManager.browseFooterAndCloseBanner();
    }

    @Test(testName = "Contact Us link test")
    public void verifyContactUsLink() throws IOException {
        LandingPageAssert.verifyContactUsLink();

    }

    @Test(testName = "Responsible Gambling link test")
    public void verifyRespGamblingLink() throws IOException {
        LandingPageAssert.verifyResponsibleGamblingLink();
    }

    @Test(testName = "Fairness and Testing link test")
    public void verifyFairnessAndTestingLink() throws IOException {
        LandingPageAssert.verifyFairnessAndTestingLink();
    }

    @Test(testName = "Licensing Bodies and Regulators link test")
    public void verifyLicBodiesAndReg() throws IOException {
        LandingPageAssert.verifyLicBodiesAndRegLink();
    }

    @Test(testName = "Sitemap link test")
    public void verifySitemapLink() throws IOException {
        LandingPageAssert.verifySitemapLink();
    }

    @Test(testName = "Privacy Policy link test")
    public void verifyPrivacyPolicyLink() throws IOException {
        LandingPageAssert.verifyPrivacyPolicyLink();
    }

    @AfterTest
    public void stopWebDriver(){
        Driver.stopWebDriver();
    }



}
